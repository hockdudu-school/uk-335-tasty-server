# API

## Auth

### Register
`/auth/register`

#### Type
Url Encoded

#### Parameters
- username: string
- password: string
- email: string

#### Response
````json
{
    "message": "Success",
    "token": "[TOKEN]"
}
````

### Token
`/auth/token`

#### Type
Url Encoded

#### Parameters
- username: string
- password: string

#### Response
````json
{
    "message": "Success",
    "token": "[TOKEN]"
}
````

## Recipe

### Featured
`/recipes/featured`


#### Response
````json
{
    "count": 0,
    "recipes": [
        {
            "id": 28,
            "name": "Pizza",
            "description": "Piizzaa",
            "images": [
                {
                    "id": "c323971e-4274-4e02-9fb4-a65d27ff7d6a",
                    "url": "http://localhost:8080/image/c323971e-4274-4e02-9fb4-a65d27ff7d6a"
                },
                {
                    "id": "f1b5bcd4-5fe0-4439-b9d7-bfa0165ff59f",
                    "url": "http://localhost:8080/image/f1b5bcd4-5fe0-4439-b9d7-bfa0165ff59f"
                }
            ],
            "view_count": 0
        }
    ]
}
````

### My recipes
`/recipes/my_recipes`

#### Response
Same as featured recipes
````json
{
    "count": 0,
    "recipes": [
        {
            "id": 28,
            "name": "Pizza",
            "description": "Piizzaa",
            "images": [
                {
                    "id": "c323971e-4274-4e02-9fb4-a65d27ff7d6a",
                    "url": "http://localhost:8080/image/c323971e-4274-4e02-9fb4-a65d27ff7d6a"
                },
                {
                    "id": "f1b5bcd4-5fe0-4439-b9d7-bfa0165ff59f",
                    "url": "http://localhost:8080/image/f1b5bcd4-5fe0-4439-b9d7-bfa0165ff59f"
                }
            ],
            "view_count": 0
        }
    ]
}
````

### Detailed
`/recipes/recipe/[ID]`

#### Response

````json
{
    "id": 29,
    "name": "Pizza",
    "description": "Piizzaa",
    "time": 1200,
    "ingredients": [
        {
            "quantity": "200g",
            "ingredient": "Tomato"
        },
        {
            "quantity": "2",
            "ingredient": "pizzas"
        }
    ],
    "preparations": [
        {
            "preparation": "Cut pizzas in half"
        },
        {
            "preparation": "Add tomato"
        }
    ],
    "view_count": 3,
    "images": [
        {
            "id": "c323971e-4274-4e02-9fb4-a65d27ff7d6a",
            "url": "http://localhost:8080/image/c323971e-4274-4e02-9fb4-a65d27ff7d6a"
        },
        {
            "id": "f1b5bcd4-5fe0-4439-b9d7-bfa0165ff59f",
            "url": "http://localhost:8080/image/f1b5bcd4-5fe0-4439-b9d7-bfa0165ff59f"
        }
    ]
}
````

### Create
`/recipes/create`

#### Type
Multipart

#### Parameters
- data: Json
- image: array (optional)

##### Data format
````json
{
    "name": "Pizza",
    "description": "Piizzaa",
    "time": 1200,
    "ingredients": [
        {
            "quantity": "200g",
            "ingredient": "Tomato"
        },
        {
            "quantity": "2",
            "ingredient": "pizzas"
        }
    ],
    "preparations": [
        {
            "preparation": "Cut pizzas in half"
        },
        {
            "preparation": "Add tomato"
        }
    ]
}
````

#### Response
See detailed recipe

### Edit
`/recipes/edit/[ID]`

#### Type
Multipart

#### Parameters
- data: Json
- image: array (optional)

##### Data format
Images that aren't on "deleted_images" aren't modified

````json
{
    "name": "Pizza",
    "description": "Piizzaa",
    "time": 1200,
    "ingredients": [
        {
            "quantity": "200g",
            "ingredient": "Tomato"
        },
        {
            "quantity": "2",
            "ingredient": "pizzas"
        }
    ],
    "preparation": [
        {
            "preparation": "Cut pizzas in half"
        },
        {
            "preparation": "Add tomato"
        }
    ],
    "deleted_images": [
        "9c309a4b-998b-4a59-9379-bcbb4ea18743",
        "105bbba4-9659-4f71-a8d4-b279664a094e",
        "0af8a6c6-ad00-4418-be22-65cd20074121",
        "c05012c4-66de-440c-a80e-56453cf89591",
        "f7a0abc8-12ae-4059-8633-d16d0c1036df",
        "e03c9ed7-751a-4414-af2e-1eb22c5a36d0"
    ]
}
````

#### Response
See detailed recipe

### Delete
`/recipes/delete/[ID]`

#### Response
````json
{
    "message": "Success"
}
````

## User

### Edit
`/user/edit`

#### Type
Url Encoded

#### Parameters
At least one must be supplied

- email: string (optional)
- password: string (optional)

### Delete
`/user/delete`

#### Response
````json
{
    "message": "Success"
}
````

## Image

### Get
`/image/[UUID]`

#### Response
Binary image

## Search

### Search
`/search?q=query`
#### Response
Same as featured recipes