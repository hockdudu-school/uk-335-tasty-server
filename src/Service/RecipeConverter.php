<?php


namespace App\Service;


use App\Entity\Image;
use App\Entity\Ingredient;
use App\Entity\Preparation;
use App\Entity\Recipe;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class RecipeConverter
{
    protected $router;

    public function __construct(RouterInterface $router, bool $isDebug)
    {
        $this->router = $router;

        if (!$isDebug) {
            $router->getContext()->setScheme('https');
        }
    }

    public function getSimpleRecipe(Recipe $recipe)
    {
        return [
            'id' => $recipe->getId(),
            'name' => $recipe->getName(),
            'description' => $recipe->getDescription(),
            'images' => $this->getRecipeImages($recipe),
            'view_count' => $recipe->getViewCount(),
        ];
    }

    public function getExtensiveRecipe(Recipe $recipe)
    {
        return [
            'id' => $recipe->getId(),
            'name' => $recipe->getName(),
            'description' => $recipe->getDescription(),
            'time' => $recipe->getTime(),
            'ingredients' => $this->getRecipeIngredients($recipe),
            'preparations' => $this->getRecipePreparations($recipe),
            'view_count' => $recipe->getViewCount(),
            'images' => $this->getRecipeImages($recipe),
        ];
    }

    public function getRecipeIngredients(Recipe $recipe)
    {
        return $recipe->getIngredients()->map(function (Ingredient $ingredient) {
            return [
                'quantity' => $ingredient->getQuantity(),
                'ingredient' => $ingredient->getIngredient(),
            ];
        })->toArray();
    }

    public function getRecipePreparations(Recipe $recipe)
    {
        return $recipe->getPreparations()->map(function (Preparation $preparation) {
            return [
                'preparation' => $preparation->getPreparation(),
            ];
        })->toArray();
    }

    public function getRecipeImages(Recipe $recipe)
    {
        return $recipe->getImages()->map(function (Image $image) {
            return [
                "id" => $image->getUuid(),
                "url" => $this->router->generate("image", ["imageUuid" => $image->getUuid()], UrlGeneratorInterface::ABSOLUTE_URL),
            ];
        })->toArray();
    }
}