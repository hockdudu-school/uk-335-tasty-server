<?php


namespace App\Entity\Listener;


use App\Entity\Image;
use App\Repository\ImageRepository;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploadListener
{
    private $imageUploadDir;
    private $logger;
    private $imageRepository;

    public function __construct(string $imageUploadDir, LoggerInterface $logger, ImageRepository $imageRepository)
    {
        $this->imageUploadDir = $imageUploadDir;
        $this->logger = $logger;
        $this->imageRepository = $imageRepository;
    }

    public function prePersist(Image $image, LifecycleEventArgs $args)
    {
        /** @var UploadedFile $file */
        if (($file = $image->getFile()) != null && $file instanceof UploadedFile) {
            if ($file->getError() != UPLOAD_ERR_OK) {
                $this->logger->error("Uploaded file is invalid", [
                    'message' => $file->getErrorMessage()
                ]);
                return;
            }

            try {

                do {
                    $uuid = Uuid::uuid4()->toString();
                } while ($this->imageRepository->findByUuid($uuid) != null);
                $image->setUuid($uuid);
            } catch (\Exception $e) {
                $this->logger->error("Couldn't generate save path for image", [
                    'exception' => $e
                ]);
            }
        }
    }

    public function postPersist(Image $image, LifecycleEventArgs $args)
    {
        if ($image->getFile() != null && $image->getFile() instanceof UploadedFile) {
            try {
                $image->getFile()->move($this->imageUploadDir, $image->getUuid());
            } catch (\Exception $e) {
                $this->logger->error("Couldn't move uploaded file", [
                    'exception' => $e
                ]);
            }
        }
    }

    public function postLoad(Image $image, LifecycleEventArgs $args)
    {
        try {
            $imageFile = new File($this->imageUploadDir . DIRECTORY_SEPARATOR . $image->getUuid());
            $image->setFile($imageFile);
        } catch (\Exception $e) {
            $this->logger->error("Couldn't load image file", [
                'exception' => $e
            ]);
        }
    }

    public function postRemove(Image $image, LifecycleEventArgs $args)
    {

        if ($image->getFile() != null) {
            if (!unlink($image->getFile()->getRealPath())) {
                $this->logger->error("Couldn't delete image file", [
                    'error' => error_clear_last()
                ]);
            }
        }
    }
}