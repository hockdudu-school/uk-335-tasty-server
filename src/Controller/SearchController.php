<?php


namespace App\Controller;


use App\Service\RecipeConverter;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Query\AbstractQuery;
use Elastica\Query\BoolQuery;
use Elastica\Query\FunctionScore;
use Elastica\Query\Nested;
use Elastica\Query\SimpleQueryString;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/search")
 */
class SearchController extends APIController
{

    private $recipeConverter;

    public function __construct(EntityManagerInterface $em, RecipeConverter $recipeConverter)
    {
        parent::__construct($em);

        $this->recipeConverter = $recipeConverter;
    }

    /**
     * @Route("")
     * @param Request $request
     * @param TransformedFinder $recipeFinder
     * @return Response
     */
    public function search(Request $request, TransformedFinder $recipeFinder)
    {
        if (!$request->query->has("q")) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $query = $request->query->get('q');

        $results = $recipeFinder->find($this->createQuery($query));

        $out = [
            'count' => sizeof($results),
            'recipes' => []
        ];

        foreach ($results as $recipe) {
            $out['recipes'][] = $this->recipeConverter->getSimpleRecipe($recipe);
        }

        return new JsonResponse($out);
    }

    protected function createQuery($query): AbstractQuery
    {
        $boolQuery = new BoolQuery();

        $nestedQueryIngredients = new Nested();
        $nestedQueryIngredients->setPath('ingredients');
        $nestedQueryIngredientsQuery = new SimpleQueryString($query);
        $nestedQueryIngredients->setQuery($nestedQueryIngredientsQuery);
        $boolQuery->addShould($nestedQueryIngredients);

        $nestedQueryPreparations = new Nested();
        $nestedQueryPreparations->setPath('preparations');
        $nestedQueryPreparationsQuery = new SimpleQueryString($query);
        $nestedQueryPreparations->setQuery($nestedQueryPreparationsQuery);
        $boolQuery->addShould($nestedQueryPreparations);

        $simpleQuery = new SimpleQueryString($query);
        $boolQuery->addShould($simpleQuery);

        // TODO: Use rank_feature when Elastica for Elasticsearch 7.x is released
        $functionScore = new FunctionScore();
        $functionScore->setQuery($boolQuery);
        $functionScore->addFieldValueFactorFunction('view_count', null, FunctionScore::FIELD_VALUE_FACTOR_MODIFIER_LOG1P);
        $functionScore->setMaxBoost(2);

        return $functionScore;
    }

}