<?php


namespace App\Controller;

use App\Entity\Image;
use App\Entity\Ingredient;
use App\Entity\Preparation;
use App\Entity\Recipe;
use App\Repository\RecipeRepository;
use App\Service\RecipeConverter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/recipes")
 */
class RecipeController extends APIController
{
    private $recipeRepository;
    private $recipeConverter;

    public function __construct(EntityManagerInterface $em, RecipeRepository $recipeRepository, RecipeConverter $recipeConverter)
    {
        parent::__construct($em);

        $this->recipeRepository = $recipeRepository;
        $this->recipeConverter = $recipeConverter;
    }

    /**
     * @Route("/featured")
     */
    public function featuredRecipes()
    {
        $recipes = $this->recipeRepository->getFeaturedRecipes();

        $out = [
            'count' => sizeof($recipes),
            'recipes' => []
        ];

        foreach ($recipes as $recipe) {
            $out['recipes'][] = $this->recipeConverter->getSimpleRecipe($recipe);
        }

        return new JsonResponse($out);
    }

    /**
     * @Route("/my_recipes")
     */
    public function myRecipes()
    {
        $recipes = $this->recipeRepository->getUserRecipes($this->getUser());

        $out = [
            'count' => sizeof($recipes),
            'recipes' => []
        ];

        foreach ($recipes as $recipe) {
            $out['recipes'][] = $this->recipeConverter->getSimpleRecipe($recipe);
        }

        return new JsonResponse($out);
    }

    /**
     * @Route("/recipe/{recipeId}", requirements={"recipeId"="\d+"})
     * @param int $recipeId
     * @return Response
     */
    public function getRecipe(int $recipeId)
    {
        $recipe = $this->recipeRepository->find($recipeId);

        if ($recipe == null) {
            return new Response(Response::HTTP_NOT_FOUND);
        }

        $this->em->beginTransaction();
        $recipe->setViewCount($recipe->getViewCount() + 1);
        $this->em->commit();
        $this->em->flush();

        return new JsonResponse($this->recipeConverter->getExtensiveRecipe($recipe));
    }

    /**
     * @Route("/create", methods={"POST"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function createRecipe(Request $request)
    {
        if (!$request->request->has('data')) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $recipeJsonBody = $request->request->get('data');

        $decodedBody = json_decode($recipeJsonBody);

        if ($decodedBody === null) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $images = [];
        if ($request->files->has('image')) {
            /** @var UploadedFile[] $images */
            $images = $request->files->get('image');
            if (!is_array($images)) {
                return new Response(Response::HTTP_BAD_REQUEST);
            }
        }

        $recipe = new Recipe();
        $this->updateRecipe($decodedBody, $recipe);

        /** @var UploadedFile $image */
        foreach ($images as $image) {
            $imageInstance = new Image();
            $imageInstance->setFile($image);
            $recipe->addImage($imageInstance);
        }

        $recipe->setUser($this->getUser());
        $this->em->persist($recipe);
        $this->em->flush();

        return new JsonResponse($this->recipeConverter->getExtensiveRecipe($recipe));
    }

    /**
     * @Route("/edit/{recipeId}", requirements={"recipeId"="\d+"}, methods={"POST"})
     * @param Request $request
     * @param int $recipeId
     * @return Response
     */
    public function editRecipe(Request $request, int $recipeId)
    {
        if (!$request->request->has('data')) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $recipeJsonBody = $request->request->get('data');

        $decodedBody = json_decode($recipeJsonBody);

        if ($decodedBody === null) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $recipe = $this->recipeRepository->find($recipeId);

        if ($recipe == null) {
            return new Response(Response::HTTP_NOT_FOUND);
        }

        if ($recipe->getUser() != $this->getUser()) {
            return new JsonResponse([
                'message' => "Can't edit messages from other users"
            ], Response::HTTP_FORBIDDEN);
        }

        $images = [];
        if ($request->files->has('image')) {
            /** @var UploadedFile[] $images */
            $images = $request->files->get('image');
            if (!is_array($images)) {
                return new Response(Response::HTTP_BAD_REQUEST);
            }
        }

        /** @var UploadedFile $image */
        foreach ($images as $image) {
            $imageInstance = new Image();
            $imageInstance->setFile($image);
            $recipe->addImage($imageInstance);
        }

        $this->updateRecipe($decodedBody, $recipe);
        $this->em->flush();

        return new JsonResponse($this->recipeConverter->getExtensiveRecipe($recipe));
    }

    /**
     * @Route("/delete/{recipeId}", requirements={"recipeId"="\d+"}, methods={"DELETE"})
     * @param int $recipeId
     * @return JsonResponse|Response
     */
    public function deleteRecipe(int $recipeId)
    {
        $recipe = $this->recipeRepository->find($recipeId);

        if ($recipe->getUser() != $this->getUser()) {
            return new Response(Response::HTTP_FORBIDDEN);
        }

        $this->em->remove($recipe);
        $this->em->flush();

        return new JsonResponse([
            'message' => 'Success'
        ]);
    }


    protected function updateRecipe($decodedBody, Recipe $recipe)
    {
        $recipe->setName($decodedBody->name);
        $recipe->setDescription($decodedBody->description);
        $recipe->setTime($decodedBody->time);

        $recipe->getIngredients()->clear();
        $recipe->getPreparations()->clear();

        foreach ($decodedBody->ingredients as $decodedIngredient) {
            $ingredient = new Ingredient();
            $ingredient->setQuantity($decodedIngredient->quantity);
            $ingredient->setIngredient($decodedIngredient->ingredient);
            $recipe->addIngredient($ingredient);
        }

        foreach ($decodedBody->preparations as $decodedPreparation) {
            $preparation = new Preparation();
            $preparation->setPreparation($decodedPreparation->preparation);
            $recipe->addPreparation($preparation);
        }

        if (key_exists('deleted_images', $decodedBody)) {
            foreach ($decodedBody->deleted_images as $imageUuid) {
                $images = $recipe->getImages()->filter(function (Image $image) use ($imageUuid) {
                    return $image->getUuid() == $imageUuid;
                });

                foreach ($images as $image) {
                    $recipe->removeImage($image);
                }
            }
        }
    }
}