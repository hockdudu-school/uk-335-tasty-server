<?php


namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends APIController
{
    /**
     * @Route("/edit", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse|Response
     */
    public function editUser(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if (!$request->request->has('email') && !$request->request->has('password')) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        /** @var User $user */
        $user = $this->getUser();

        if ($request->request->has('email')) {
            if (!filter_var($request->request->get('email'), FILTER_VALIDATE_EMAIL)) {
                return new JsonResponse([
                    'message' => 'Email is invalid'
                ], Response::HTTP_BAD_REQUEST);
            }

            $user->setEmail($request->request->get('email'));
        }

        if ($request->request->has('password')) {
            $user->setPassword($passwordEncoder->encodePassword(
                $user,
                $request->request->get('password')
            ));
        }

        $this->em->flush();

        return new JsonResponse([
            'message' => 'Success'
        ]);
    }

    /**
     * @Route("/delete", methods={"DELETE"})
     */
    public function delete()
    {
        $user = $this->getUser();
        $this->em->remove($user);
        $this->em->flush();

        return new JsonResponse([
            'message' => 'Success'
        ]);
    }
}