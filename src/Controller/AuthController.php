<?php


namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/auth")
 */
class AuthController extends APIController
{
    private $userRepository;
    private $passwordEncoder;

    // 30 days
    private $expirationTime = 60 * 60 * 24 * 30;

    public function __construct(EntityManagerInterface $em, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($em);
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/register", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function registerUser(Request $request)
    {
        if (!$request->request->has('username') ||
            !$request->request->has('email') ||
            !$request->request->has('password')
        ) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        if (!filter_var($request->request->get('email'), FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse([
                'message' => 'Email is invalid'
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($this->userRepository->findByUsername($request->request->get('username')) != null) {
            return new JsonResponse([
                'message' => 'Username exists'
            ], Response::HTTP_CONFLICT);
        }

        if ($this->userRepository->findByEmail($request->request->get('email')) != null) {
            return new JsonResponse([
                'message' => 'Email exists'
            ], Response::HTTP_CONFLICT);
        }

        $user = new User();
        $user->setUsername($request->request->get('username'));
        $user->setEmail($request->request->get('email'));

        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $request->request->get('password')
        ));

        $this->em->persist($user);
        $this->em->flush();

        return new JsonResponse([
            'message' => 'Success',
            'token' => $this->getNewToken($user)
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/token", methods={"POST"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function getToken(Request $request)
    {
        if (!$request->request->has('username') ||
            !$request->request->has('password')
        ) {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $user = $this->userRepository->findByUsername($request->request->get('username'));

        if ($user == null || !$this->passwordEncoder->isPasswordValid($user, $request->request->get('password'))) {
            return new JsonResponse([
                'message' => 'Username or password are invalid'
            ], Response::HTTP_UNAUTHORIZED);
        }

        return new JsonResponse([
            'message' => 'Success',
            'token' => $this->getNewToken($user)
        ]);
    }

    /**
     * Gets a new token for the user
     * @param User $user
     * @return string
     */
    protected function getNewToken(User $user): string
    {
        $now = time();
        $expiration = $now + $this->expirationTime;
        return JWT::encode([
            'id' => $user->getId(),
            'exp' => $expiration,
            'iat' => $now
        ], $_ENV['JWT_KEY']);
    }
}