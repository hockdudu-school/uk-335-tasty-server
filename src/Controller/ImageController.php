<?php


namespace App\Controller;


use App\Repository\ImageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/image")
 */
class ImageController extends APIController
{
    private $imageRepository;

    public function __construct(EntityManagerInterface $em, ImageRepository $imageRepository)
    {
        parent::__construct($em);

        $this->imageRepository = $imageRepository;
    }

    /**
     * @Route("/{imageUuid}", name="image")
     * @param string $imageUuid
     * @return BinaryFileResponse|Response
     */
    public function getImage(string $imageUuid)
    {
        $image = $this->imageRepository->findByUuid($imageUuid);

        if ($image == null) {
            return new Response(Response::HTTP_NOT_FOUND);
        }

        if ($image->getFile() == null) {
            return new Response(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new BinaryFileResponse($image->getFile());
    }

}